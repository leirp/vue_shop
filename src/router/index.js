import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "../views/Login";
import Home from "../views/home/Home";
import Welcome from "../views/home/welcome/Welcome";
import Users from "../views/home/users/Users";
import Rights from "../views/home/rights/Rights";
import Roles from "../views/home/rights/Roles";
import Categories from "../views/home/goods/Categories";
import Params from "../views/home/goods/Params";
import List from "../views/home/goods/List";
import Add from "../views/home/goods/Add";
import Orders from "../views/home/orders/Orders";
import Report from "../views/home/report/Report";

Vue.use(VueRouter)

const routes = [
  {path:"/",redirect:"/login"},
  {path:"/login",component:Login},
  {
    path:"/home",
    component:Home,
    redirect:"/welcome",
    children:[
      {path: "/welcome", component:Welcome},
      {path: "/users", component: Users},
      {path:"/rights", component:Rights},
      {path:"/roles", component:Roles},
      {path:"/categories", component:Categories},
      {path:"/params", component:Params},
      {path:'/goods',component:List},
      {path:'/goods/add',component:Add},
      {path:'/orders',component:Orders},
      {path:'/reports',component:Report}

    ]
  }
]

const router = new VueRouter({
  routes
})
router.beforeEach((to,from,next)=>{
  if (to.path === '/login') {
    return next()
  }

  const token = window.sessionStorage.getItem('token')
  if (!token) {
    return next('/login')
  } else {
    return next()
  }

})
export default router
